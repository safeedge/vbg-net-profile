#!/usr/bin/env python2

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the InstaGENI library.
import geni.rspec.igext as ig
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal object,
pc = portal.Context()

hostImage = 'urn:publicid:IDN+emulab.net+image+emulab-ops//XEN46-64-STD'
guestImage = 'urn:publicid:IDN+emulab.net+image+SafeEdge//ENTPNT-VBG'
internalGuestImage = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD'
controllerImage = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD'
flowopsImage = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD'

pc.defineParameter("vmsPerHost","VBG VMs per Host",portal.ParameterType.INTEGER,2,
                   longDescription="Number of VBG VMs per physical host.")
pc.defineParameter("hosts","Physical Hosts",portal.ParameterType.INTEGER,1,
                   longDescription="Number of physical hosts.  If zero, VMs will not be fixed to physical host machines; they will be best-fit mapped.")
pc.defineParameter("hostType","Physical Host Node Type",portal.ParameterType.NODETYPE,"d430",
                   [('','Unspecified'),('d430','d430'),('d710','d710'),('d820','d820'),('pc3000','pc3000'),('m510','m510'),('xl170','xl170')],
                   longDescription="Type of physical host nodes.")
pc.defineParameter("onlyVMs","No controller/flowops nodes",portal.ParameterType.BOOLEAN,False,
                   longDescription="Do not include controller/flowops nodes, only VMs.")
pc.defineParameter("coresPerVM","Cores per VBG VM",portal.ParameterType.INTEGER,1,
                   longDescription="Number of cores each VBG VM will get.")
pc.defineParameter("ramPerVM","RAM per VBG VM",portal.ParameterType.INTEGER,2048,
                   longDescription="Amount of RAM each VBG VM will get.")
pc.defineParameter("startupCmd","VBG VM Startup Command",portal.ParameterType.STRING,"",
                   longDescription="A command each VBG VM will run on startup.")
pc.defineParameter("mgmtLinkSpeed", "Mgmt LAN Speed",portal.ParameterType.INTEGER, 1000000,
                   [(0,"Any"),(100000,"100Mb/s"),(1000000,"1Gb/s"),(10000000,"10Gb/s")],
                   longDescription="A specific link speed to use for the mgmt LAN.")
pc.defineParameter("mgmtLinkLatency", "Mgmt LAN Latency",portal.ParameterType.LATENCY, 0,
                   longDescription="A specific latency to use for the mgmt LAN.")

pc.defineParameter("numberInternalHosts","Number VM hosts plugged into each VBG VM",
                   portal.ParameterType.INTEGER,0,
                   longDescription="Number of in-home VMs plugged into VBG GigabitEthX ports.")
pc.defineParameter("internalHostLinkSpeed", "Internal Host Link Speed",
                   portal.ParameterType.INTEGER, 1000000,
                   [(0,"Any"),(100000,"100Mb/s"),(1000000,"1Gb/s"),(10000000,"10Gb/s")],
                   longDescription="The link speed to use for each internal host<->VBG link.")
pc.defineParameter("internalHostLinkLatency", "Internal Host Link Latency",
                   portal.ParameterType.LATENCY, 0,
                   longDescription="A specific latency to use for each internal host<->VBG link.")
#pc.defineParameter("linkLoss", "LAN/Link Loss",portal.ParameterType.FLOAT, 0.0,
#                   longDescription="A specific loss rate to use for each LAN and link.")
pc.defineParameter("multiplex", "Multiplex Networks",portal.ParameterType.BOOLEAN, True,
                   longDescription="Multiplex all LANs and links.")
pc.defineParameter("bestEffort", "Best Effort Link Bandwidth",portal.ParameterType.BOOLEAN, True,
                   longDescription="Do not require guaranteed bandwidth throughout switch fabric.")
pc.defineParameter("trivialOk", "Trivial Links Ok",portal.ParameterType.BOOLEAN, True,
                   longDescription="Maybe use trivial links.")
#pc.defineParameter("forceVlanLinkType", "Force VLAN Link Type",portal.ParameterType.BOOLEAN, False,
#                   longDescription="Force the type of each LAN/Link to vlan.")
pc.defineParameter("ipAllocationStrategy","IP Addressing",
                   portal.ParameterType.STRING,"script",[("cloudlab","CloudLab"),("script","This Script")],
                   longDescription="Either let CloudLab auto-generate IP addresses for the nodes in your networks, or let this script generate them.  If you include nodes at multiple sites, you must choose this script!  The default is this script, because the subnets CloudLab generates for flat networks are sized according to the number of physical nodes in your topology.  If the script IP address generation is buggy or otherwise insufficient, you can fall back to CloudLab and see if that improves things.")
pc.defineParameter("ipAllocationUsesCIDR","Script IP Addressing Uses CIDR",
                   portal.ParameterType.BOOLEAN,False,
                   longDescription="If this script is generating the IP addresses for your nodes, and this button is checked, we will generate a minimal set of subnets that are just large enough to contain the hosts in each subnet.  If it is not checked, we will generate class B/C subnets inside the 10.0.0.0/8 class A unrouted subnet, for each separate link/lan.")

params = pc.bindParameters()

if params.hosts < 0:
    pc.reportError(portal.ParameterError("Must specify zero or more physical hosts",['hosts']))
if params.vmsPerHost < 1:
    pc.reportError(portal.ParameterError("Must specify at least one VM per host",['vmsPerHost']))
if params.coresPerVM < 0:
    pc.reportError(portal.ParameterError("Must specify at least one core per VM",['coresPerVM']))
if params.ramPerVM < 0:
    pc.reportError(portal.ParameterError("Must specify at least one core per VM",['ramPerVM']))


# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

tour = ig.Tour()
tour.Description(
    ig.Tour.TEXT,"Instantiate a LAN of VBGs, also containing flowops and controller nodes.")
request.addTour(tour)

lans = list()
links = list()
vhosts = list()
vnodes = list()
nodes = list()
ifaces = dict()

#
# Create the mgmt LAN (Sfp1)
#
mgmtlan = pg.LAN('mgmtlan')
if params.trivialOk is True:
    mgmtlan.trivial_ok = True
else:
    mgmtlan.trivial_ok = False
if params.bestEffort is True:
    mgmtlan.best_effort = True
else:
    mgmtlan.best_effort = False
if params.multiplex is True:
    mgmtlan.link_multiplexing = True
else:
    mgmtlan.link_multiplexing = False
#if params.forceVlanLinkType:
#    mgmtlan.type = "vlan"
mgmtlan.vlan_tagging = True
if params.mgmtLinkSpeed > 0:
    mgmtlan.bandwidth = params.mgmtLinkSpeed
if params.mgmtLinkLatency > 0:
    mgmtlan.latency = params.mgmtLinkLatency
#if params.linkLoss > 0:
#    mgmtlan.loss = params.linkLoss
lans.append(mgmtlan)
ifaces[mgmtlan.client_id] = []

if not params.onlyVMs:
    #
    # Create the controller and flowops nodes.
    #
    node = pg.RawPC('controller')
    node.disk_image = controllerImage
    if params.hostType:
        node.hardware_type = params.hostType
    iface = node.addInterface('ifMgmt')
    mgmtlan.addInterface(iface)
    ifaces[mgmtlan.client_id].append(iface)
    nodes.append(node)

    node = pg.RawPC('flowops')
    node.disk_image = flowopsImage
    if params.hostType:
        node.hardware_type = params.hostType
    iface = node.addInterface('ifMgmt')
    mgmtlan.addInterface(iface)
    ifaces[mgmtlan.client_id].append(iface)
    nodes.append(node)

#
# Create the vhosts, if desired.
#
for i in range(0,params.hosts):
    host = pg.RawPC('vhost-%d' % (i,))
    host.exclusive = True
    if params.hostType:
        host.hardware_type = params.hostType
    host.disk_image = hostImage
    vhosts.append(host)
    pass

#
# Create the VBG VMs.
#
vbgs = []
if params.hosts > 0:
    for i in range(0,params.hosts):
        thl = []
        for j in range(0,params.vmsPerHost):
            vname = '%d-%d' % (i,j)
            thl.append(vname)
            vbgs.append((vname,'vhost-%d' % (i,)))
    pass
else:
    thl = []
    for j in range(0,params.vmsPerHost):
        vname = '%d' % (j,)
        thl.append(vname)
        vbgs.append((vname,None))
    pass

for (vnamefrag,vhostname) in vbgs:
    node = ig.XenVM('vbg-%s' % (vnamefrag,))
    if params.coresPerVM > 0:
        node.cores = params.coresPerVM
    if params.ramPerVM:
        node.ram = params.ramPerVM
    if params.hostType:
        node.xen_ptype = '%s-vm' % (params.hostType,)
    if vhostname:
        node.InstantiateOn(vhostname)
    node.exclusive = True
    node.disk_image = guestImage
    if params.startupCmd:
        node.addService(pg.Execute(shell="sh",command=params.startupCmd))
    iface = node.addInterface('ifMgmt')
    ifrename_val = "mgmtlan:Sfp1"
    mgmtlan.addInterface(iface)
    ifaces[mgmtlan.client_id].append(iface)
    # If there are internal home hosts hanging off the VBG, create those
    # now.
    for i in range(0,params.numberInternalHosts):
        inode = ig.XenVM('home-node-%s-%d' % (vnamefrag,i))
        if vhostname:
            inode.InstantiateOn(vhostname)
        inode.disk_image = internalGuestImage
        inode.cores = 1
        inode.ram = 1024
        lname = 'ilink-%s-%d' % (vnamefrag,i)
        ilink = pg.LAN(lname)
        if params.trivialOk is True:
            ilink.trivial_ok = True
        else:
            ilink.trivial_ok = False
        if params.bestEffort is True:
            ilink.best_effort = True
        else:
            ilink.best_effort = False
        if params.multiplex is True:
            ilink.link_multiplexing = True
        else:
            ilink.link_multiplexing = False
        #if params.forceVlanLinkType:
        #    ilink.type = "vlan"
        ilink.vlan_tagging = True
        if params.internalHostLinkSpeed > 0:
            ilink.bandwidth = params.internalHostLinkSpeed
        if params.internalHostLinkLatency > 0:
            ilink.latency = params.internalHostLinkLatency
        #if params.linkLoss > 0:
        #    ilink.loss = params.linkLoss
        siface = inode.addInterface('if0')
        diface = node.addInterface('ifInt%d' % (i,))
        ifrename_val = "%s,%s:GigabitEth%i" % (ifrename_val,lname,i+1)
        ilink.addInterface(siface)
        ilink.addInterface(diface)
        ifaces[ilink.client_id] = []
        ifaces[ilink.client_id].append(siface)
        ifaces[ilink.client_id].append(diface)
        links.append(ilink)
        vnodes.append(inode)
        pass
    node.Attribute('XEN_FORCE_HVM','1')
    node.Attribute('IFRENAME',ifrename_val)
    vnodes.append(node)
    pass

baseaddr = [10,0,0,0]
basebits = 8
minsubnetbits = basebits + 1
maxsubnetbits = 32 - 2
networks = dict()
netmasks = dict()
lastaddr = dict()
# The list of allowed subnet bit sizes.
ALLOWED_SUBNETS = []
# An ordered (increasing bit size) list of (bitsize,networkname) tuples.
REQUIRED_SUBNETS = []
# A dict of key networkname and value { 'bits':bitsize,'network':networkint,
#   'hostnum':hostint,'maxhosts':maxhostsint,'netmaskstr':'x.x.x.x' }
SUBNETS = {}

if params.ipAllocationUsesCIDR:
    subbits = basebits + 1
    while subbits <= maxsubnetbits:
        ALLOWED_SUBNETS.append(subbits)
        subbits += 1
else:
    ALLOWED_SUBNETS = [ 16,24 ]

def __htoa(h):
    return "%d.%d.%d.%d" % (
        (h >> 24) & 0xff,(h >> 16) & 0xff,(h >> 8) & 0xff,h & 0xff)

def ipassign_request_network(name,numhosts):
    #
    # Here, we just reserve each network's bitspace, in increasing
    # bitsize.  This makes eventual assignment trivial; we just pick the
    # next hole out of the previous (larger or equiv size) subnet.
    #
    _sz = numhosts + 1
    i = 0
    while _sz > 0:
        i += 1
        _sz = _sz >> 1
    # The minimum network size is a 255.255.255.252 (a /30).
    if i < 2:
        i = 2
    # Change the host bitsize to a network bitsize.
    i = 32 - i
    sn = None
    for _sn in ALLOWED_SUBNETS:
        if i < _sn:
            break
        sn = _sn
    if sn == None:
        raise Exception("network of bitsize %d (%d hosts) cannot be supported"
                        " with basebits %d" % (i,numhosts,basebits))
    ipos = 0
    for (bitsize,_name) in REQUIRED_SUBNETS:
        if sn < bitsize:
            break
        ipos += 1
    REQUIRED_SUBNETS.insert(ipos,(sn,name))
    return

def ipassign_assign_networks():
    basenum = baseaddr[0] << 24 | baseaddr[1] << 16 \
        | baseaddr[2] << 8 | baseaddr[3]
    # If any of our network broadcast addrs exceed this address, we've
    # overallocated.
    nextbasenum = basenum + 2 ** (32 - basebits)

    #
    # Process REQUIRED_SUBNETS list into SUBNETS dict, and setup the
    # next host number.  Again, we assume that REQUIRED_SUBNETS is
    # ordered in increasing number of network bits required.
    #
    lastbitsize = None
    lastnetnum = None
    for i in range(0,len(REQUIRED_SUBNETS)):
        (bitsize,name) = REQUIRED_SUBNETS[i]
        if lastbitsize != bitsize and lastbitsize != None:
            # Need to get into the next lastbitsize subnet to allocate
            # for the bitsize subnet.
            lastnetnum += 2 ** (32 - lastbitsize)

        if lastnetnum != None:
            netnum = lastnetnum + 2 ** (32 - bitsize)
        else:
            netnum = basenum
        if netnum >= nextbasenum:
            raise Exception("out of network space in /%d at network %s (/%d)"
                            % (basebits,name,bitsize))
        netmask = netnum + (2 ** (32 - bitsize)) - 1
        SUBNETS[name] = {
            'bits':bitsize,'networknum':netnum,'networkstr':__htoa(netnum),
            'hostnum':0,'maxhosts':2 ** (32 - bitsize) - 1,'netmask':netmask,
            'netmaskstr':__htoa(netmask),'addrs':[]
        }
        lastnetnum = netnum
        lastbitsize = bitsize

def ipassign_assign_host(lan):
    #
    # Assign an IP address in the given lan.  If you call this function
    # more times than you effectively promised (by calling
    # ipassign_add_network with a requested host size), you will be
    # handed an Exception.
    #
    bitsize = None
    for (_bitsize,_name) in REQUIRED_SUBNETS:
        if _name == lan:
            bitsize = _bitsize
    if not bitsize:
        raise Exception("unknown network name %s" % (lan,))
    if not _name in SUBNETS:
        raise Exception("unknown network name %s" % (lan,))

    maxhosts = (2 ** bitsize - 1)
    if SUBNETS[lan]['hostnum'] >= maxhosts:
        raise Exception("no host space left in network name %s (max hosts %d)"
                        % (lan,maxhosts))

    SUBNETS[lan]['hostnum'] += 1

    addr = __htoa(SUBNETS[lan]['networknum'] + SUBNETS[lan]['hostnum'])
    if True:
        SUBNETS[lan]['addrs'].append(addr)
    return (addr,SUBNETS[lan]['netmaskstr'])

#
# Assign IP addresses if necessary.
#
if params.ipAllocationStrategy == 'script':
    for lan in lans:
        name = lan.client_id
        ipassign_request_network(name,len(ifaces[name]))
    for link in links:
        name = link.client_id
        ipassign_request_network(name,len(ifaces[name]))
    ipassign_assign_networks()
    for lanlink in ifaces.keys():
        for iface in ifaces[lanlink]:
            (address,netmask) = ipassign_assign_host(lanlink)
            iface.addAddress(pg.IPv4Address(address,netmask))
    #for name in SUBNETS.keys():
    #    print "SUBNET %s: %s %s/%d %s" % (
    #        name,SUBNETS[name]['netmaskstr'],SUBNETS[name]['networkstr'],
    #        SUBNETS[name]['bits'],str(SUBNETS[name]['addrs']))
    pass

for x in vhosts:
    request.addResource(x)
for x in vnodes:
    request.addResource(x)
for x in nodes:
    request.addResource(x)
for x in lans:
    request.addResource(x)
for x in links:
    request.addResource(x)

pc.printRequestRSpec(request)
